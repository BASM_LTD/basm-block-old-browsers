/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2016 Basm Ltd http://www.basm.co.uk
 * Registered in the UK No. 9051496
 * 
 * basm-block-old-browsers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * basm-block-old-browsers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with basm-block-old-browsers.  If not, see <http://www.gnu.org/licenses/>. * 
 */

package com.basm.atlas.confluence.plugin.cache;

import java.util.ArrayList;
import java.util.HashMap;

public class BrowserCache
{
  private static HashMap<String, ArrayList<BrowserModel>> browserCache = new HashMap<String, ArrayList<BrowserModel>>();

  public static void invalidate()
  {
    browserCache.clear();
  }

  public static void put(String browserName, BrowserModel browser)
  {
    ArrayList<BrowserModel> entries = browserCache.get(browserName);
    if (entries == null)
    {
      entries = new ArrayList<BrowserModel>();
    }
    entries.add(browser);
    browserCache.put(browserName, entries);
  }

  public static ArrayList<BrowserModel> get(String browserName)
  {
    return browserCache.get(browserName);
  }
}
