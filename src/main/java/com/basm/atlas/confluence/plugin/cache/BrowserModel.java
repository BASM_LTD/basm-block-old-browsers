/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2016 Basm Ltd http://www.basm.co.uk
 * Registered in the UK No. 9051496
 * 
 * basm-block-old-browsers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * basm-block-old-browsers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with basm-block-old-browsers.  If not, see <http://www.gnu.org/licenses/>. * 
 */

package com.basm.atlas.confluence.plugin.cache;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BrowserModel
{
  @XmlElement
  private String name;

  @XmlElement
  private String version;

  @XmlElement
  private String type;

  @XmlElement
  private boolean before;

  @XmlElement
  private boolean enabled;

  public BrowserModel()
  {
  }

  public BrowserModel(String name, String version, boolean before, String type, boolean enabled)
  {
    this.name = name;
    this.version = version;
    this.before = before;
    this.type = type;
    this.enabled = enabled;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getVersion()
  {
    return version;
  }

  public void setVersion(String version)
  {
    this.version = version;
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public boolean isBefore()
  {
    return before;
  }

  public void setBefore(boolean before)
  {
    this.before = before;
  }

  public boolean isEnabled()
  {
    return enabled;
  }

  public void setEnabled(boolean enabled)
  {
    this.enabled = enabled;
  }

  @Override
  public String toString()
  {
    return "BrowserModel [name=" + name + ", version=" + version + ", type=" + type + ", before=" + before + ", enabled=" + enabled
        + "]";
  }
}
