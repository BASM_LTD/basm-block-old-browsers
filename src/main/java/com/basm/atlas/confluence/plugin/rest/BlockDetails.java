package com.basm.atlas.confluence.plugin.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BlockDetails
{
  @XmlElement
  public String type;

  @XmlElement
  public String message;

  public BlockDetails()
  {
    type = "";
    message = "";
  }

  public String getType()
  {
    return type;
  }

  public void setType(String type)
  {
    this.type = type;
  }

  public String getMessage()
  {
    return message;
  }

  public void setMessage(String message)
  {
    this.message = message;
  }

  @Override
  public String toString()
  {
    return "BlockDetails [type=" + type + ", message=" + message + "]";
  }
}
