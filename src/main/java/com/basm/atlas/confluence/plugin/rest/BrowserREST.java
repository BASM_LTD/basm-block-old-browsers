package com.basm.atlas.confluence.plugin.rest;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.basm.atlas.confluence.plugin.service.BrowserService;
import com.basm.atlas.confluence.plugin.service.BrowserServiceImpl;

import eu.bitwalker.useragentutils.BrowserType;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;

@Path("/")
public class BrowserREST
{
  private static final Logger log = LoggerFactory.getLogger(BrowserServiceImpl.class);

  private final BrowserService browserService;

  public BrowserREST(BrowserService browserService)
  {
    Log.debug("REST Endpoint loaded");
    this.browserService = browserService;
  }

  @GET
  @Produces(
  { MediaType.APPLICATION_JSON })
  public Response validate(@HeaderParam("user-agent") String userAgentText)
  {
    log.debug("Hit endpoint, useragent = " + userAgentText);
    BlockDetails details = new BlockDetails();
    UserAgent userAgent = UserAgent.parseUserAgentString(userAgentText);
    if (userAgent.getBrowser().getBrowserType() == BrowserType.WEB_BROWSER)
    {
      String browserName = userAgent.getBrowser().getGroup().getName().toUpperCase();
      Version browserVersion = userAgent.getBrowserVersion();
      details = browserService.getAction(browserName, browserVersion);
    }
    return Response.ok(details).build();
  }

}