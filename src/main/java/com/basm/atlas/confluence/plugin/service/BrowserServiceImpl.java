/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2016 Basm Ltd http://www.basm.co.uk
 * Registered in the UK No. 9051496
 * 
 * basm-block-old-browsers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * basm-block-old-browsers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with basm-block-old-browsers.  If not, see <http://www.gnu.org/licenses/>. * 
 */

package com.basm.atlas.confluence.plugin.service;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.basm.atlas.confluence.plugin.cache.BrowserCache;
import com.basm.atlas.confluence.plugin.cache.BrowserModel;
import com.basm.atlas.confluence.plugin.rest.BlockDetails;

import eu.bitwalker.useragentutils.Version;

public class BrowserServiceImpl implements BrowserService
{
  private static final Logger log = LoggerFactory.getLogger(BrowserServiceImpl.class);

  private final ActiveObjects ao;
  private final PluginSettingsFactory pluginSettingsFactory;
  private static final String PLUGIN_STORAGE_KEY = "com.basm.atlas.confluence.plugin";
  private static final String BROWSERS = PLUGIN_STORAGE_KEY + ".browsers";
  private static final String WARNING_TEXT = PLUGIN_STORAGE_KEY + ".warningText";
  private static final String BLOCK_TEXT = PLUGIN_STORAGE_KEY + ".blockText";
  private static final String SEPARATOR = "/";
  private static final String BEFORE = "BEFORE";

  private PluginSettings pluginSettings;

  public BrowserServiceImpl(final ActiveObjects ao, PluginSettingsFactory pluginSettingsFactory)
  {
    log.debug("servlet loaded");
    this.ao = ao;
    this.pluginSettingsFactory = pluginSettingsFactory;
    pluginSettings = pluginSettingsFactory.createGlobalSettings();
    populateCache();
  }

  public ArrayList<BrowserModel> getBrowserDetails(String browserName)
  {
    ArrayList<BrowserModel> entries = BrowserCache.get(browserName);
    return entries;
  }

  public void setConfig(String browsers, String warningText, String blockText)
  {
    pluginSettings.put(BROWSERS, browsers);
    pluginSettings.put(WARNING_TEXT, warningText);
    pluginSettings.put(BLOCK_TEXT, blockText);
    populateCache();
  }

  public String getBrowsers()
  {
    return (String) pluginSettings.get(BROWSERS);
  }

  public String getWarningText()
  {
    return (String) pluginSettings.get(WARNING_TEXT);
  }

  public String getBlockText()
  {
    return (String) pluginSettings.get(BLOCK_TEXT);
  }

  public BlockDetails getAction(String browserName, Version version)
  {
    log.debug("browserName = " + browserName + "  version = " + version.getMajorVersion() + "  -  " + version.getMinorVersion());
    BlockDetails details = new BlockDetails();
    ArrayList<BrowserModel> entries = getBrowserDetails(browserName);
    boolean match = false;
    if (entries != null)
    {
      for (BrowserModel current : entries)
      {
        // check if this entry applies

        Version currentVersion = new Version(current.getVersion(), null, null);
        if (current.isBefore())
        {
          if (version.compareTo(currentVersion) < 0)
          {
            match = true;
          }
        }
        else
        {
          if (version.compareTo(currentVersion) > 0)
          {
            match = true;
          }
        }
        if (match)
        {
          if (current.getType().equalsIgnoreCase("WARN"))
          {
            details.setMessage(getWarningText());
            details.setType(current.getType());
          }
          else if (current.getType().equalsIgnoreCase("BLOCK"))
          {
            details.setMessage(getBlockText());
            details.setType(current.getType());
          }
        }
      }
    }
    return details;
  }

  private void populateCache()
  {
    BrowserCache.invalidate();
    try
    {
      BufferedReader bufReader = new BufferedReader(new StringReader(getBrowsers()));
      String line = null;
      while ((line = bufReader.readLine()) != null)
      {
        if (line.trim().length() < 1)
        {
          // discard empty lines
        }
        else if (StringUtils.countMatches(line, SEPARATOR) != 3)
        {
          log.error("invalid entry found, discarding this line :- " + line);
        }
        else
        {
          StringTokenizer stringTokenizer = new StringTokenizer(line, SEPARATOR);
          try
          {
            String name = stringTokenizer.nextToken().toUpperCase();
            String version = stringTokenizer.nextToken();
            String beforeString = stringTokenizer.nextToken();
            boolean before = (beforeString.trim().equalsIgnoreCase(BEFORE)) ? true : false;
            String type = stringTokenizer.nextToken().toUpperCase();
            BrowserModel browserModel = new BrowserModel(name, version, before, type, true);
            log.debug("Parsed Browser Model is {}", browserModel);
            BrowserCache.put(name, browserModel);
          }
          catch (Exception e)
          {
            log.error("!!! unable to parse line " + line, e);
          }
        }
      }
    }
    catch (Exception e)
    {
      log.error("Failed to populate cache", e);
    }
  }
}
