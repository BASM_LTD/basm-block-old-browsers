/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2016 Basm Ltd http://www.basm.co.uk
 * Registered in the UK No. 9051496
 * 
 * basm-block-old-browsers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * basm-block-old-browsers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with basm-block-old-browsers.  If not, see <http://www.gnu.org/licenses/>. * 
 */

package com.basm.atlas.confluence.plugin.servlet;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.basm.atlas.confluence.plugin.service.BrowserService;

public class ConfigureBrowsersServlet extends HttpServlet
{
  private static final long serialVersionUID = 1L;

  private final UserManager userManager;
  private final LoginUriProvider loginUriProvider;
  private final TemplateRenderer renderer;
  private final BrowserService browserService;

  public ConfigureBrowsersServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer renderer,
      BrowserService browserService)
  {
    this.userManager = userManager;
    this.loginUriProvider = loginUriProvider;
    this.renderer = renderer;
    this.browserService = browserService;
    Log.debug("servlet loaded");
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    String username = userManager.getRemoteUsername(request);
    if (username == null || !userManager.isSystemAdmin(username))
    {
      redirectToLogin(request, response);
      return;
    }

    sendToAdminPage(response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    String browsers = request.getParameter("browsers");
    String warningText = request.getParameter("warningText");
    String blockText = request.getParameter("blockText");
    browserService.setConfig(browsers, warningText, blockText);
    sendToAdminPage(response);
  }

  private void sendToAdminPage(HttpServletResponse response) throws IOException
  {
    Map<String, Object> context = new HashMap<String, Object>();
    context.put("browsers", browserService.getBrowsers());
    context.put("warningText", browserService.getWarningText());
    context.put("blockText", browserService.getBlockText());
    response.setContentType("text/html;charset=utf-8");
    renderer.render("templates/block-old-browsers.vm", context, response.getWriter());

  }

  private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
  {
    response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
  }

  private URI getUri(HttpServletRequest request)
  {
    StringBuffer builder = request.getRequestURL();
    if (request.getQueryString() != null)
    {
      builder.append("?");
      builder.append(request.getQueryString());
    }
    return URI.create(builder.toString());
  }

}