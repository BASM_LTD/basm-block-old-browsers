AJS.toInit(function ($) {
  $.ajax({
    url: AJS.contextPath() + "/rest/browser/1.0/",
    type: "GET",
    dataType: "json",
    error:function () {
      console.log("failed");
    },
    success: function (response) {
   	  if (response.type == "WARN") {
          var myFragment = soy.renderAsFragment(com.basm.block.soy.warningBanner, response);
          $('body').prepend(myFragment);
   	  } else if (response.type == "BLOCK") {
          var myFragment = soy.renderAsFragment(com.basm.block.soy.block, response);
          $('body').prepend(myFragment);
          AJS.dialog2("#block-dialog").show();
     }
   	}
  });
});



